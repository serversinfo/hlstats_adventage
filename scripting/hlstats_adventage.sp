#pragma semicolon 1
#include <SteamWorks>
#pragma newdecls required
#include <sourcemod>
#include <groupplayers>
#include <warden>
//#include <connecthook>
#define game "jailcsgo"		// "wcsgo" "surf" "zm"


/*ConnectToDB*/
Handle g_Mysql;
int g_iUId[MAXPLAYERS+1];
bool g_bUIDConnd[4096];
char g_sOldName[64][MAXPLAYERS+1];
char sInGr[64][MAXPLAYERS+1];

//char g_sCalled[32][10];		// будут записываться 10 последних стимидов которые первый раз подключились
//int g_iNextCalled;			// чтобы помнить в какую ячейку писать следующий стимид


#define DEBUG
#if defined DEBUG
stock void debugMessage(const char[] message, any ...)
{
	char szMessage[256], szPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, szPath, sizeof(szPath), "logs/debug_statsx.txt");
	VFormat(szMessage, sizeof(szMessage), message, 2);
	LogToFile(szPath, szMessage);
}
#define dbgMsg(%0) debugMessage(%0)
#else
#define dbgMsg(%0)
#endif


/*Client*/
int g_iPlayedTime[MAXPLAYERS+1] = {-1, ...};		// если -1 значит время не запрашивалось, например если плагин перезагрузили

public Plugin myinfo=
{
	name	= "[ANY]HlStatsX More Info (PRIVATE)",
	author	= "Darkeneez & ShaRen",
	version = "1.2",
	url 	= "vk.com/darkeneez97",
};

public void OnPluginStart()
{
	ConnectToMySql();
	HookEvent("player_connect", eV_player_connect);
	CreateTimer(10.0, Check, _, TIMER_REPEAT);
	RegAdminCmd("statsx", Hlstatsx_Test, ADMFLAG_BAN);
}

public Action Hlstatsx_Test(int client, int args)
{
	bool bWritten[MAXPLAYERS+1];
	int count, conn;
	int alltime;
	int inG, iS, iNoS;
	for(int i=1; i<=MaxClients; i++) {
		if (IsClientInGame(i)) {
			if(!IsClientSourceTV(i)) {
				alltime = alltime + g_iPlayedTime[i];
				count++;
			}
		} else if (IsClientConnected(i))
			conn++;
		if(IsClientInGame(i) && !IsFakeClient(i)) {
			if (SteamWorks_HasLicenseForApp(i, 730) == k_EUserHasLicenseResultHasLicense) {
				SteamWorks_GetUserGroupStatus(i, 103582791431452545);
				if(InGroup(i)) {
					SteamWorks_HasLicenseForApp(i, 624820) ? (sInGr[i] = "[G]f"):(sInGr[i] = "[G] ");
					inG++;
				} else SteamWorks_HasLicenseForApp(i, 624820) ? (sInGr[i] = "[s]f"):(sInGr[i] = "[s] ");
				iS++;
			} else {
				sInGr[i] = "    ";
				iNoS++;
			}
		}
	}
	int iCount = count;
	for( ;count;count--) {
		int max;
		for(int i=1; i<=MaxClients; i++)
			if (g_iPlayedTime[i] > g_iPlayedTime[max] && !bWritten[i])
				max = i;
		bWritten[max] = true;
		if (g_iPlayedTime[max] != -1)
			PrepareTime(client, max, g_iPlayedTime[max]);
		else PrintToConsole(client, "%N - не обработан", max);
	}
	PrepareTime(client, 0, alltime, iCount);
	PrintToConsole(client, "В группе %i/%i, остальные %i No-Steam, подключаются %i", inG, iS, iNoS, conn);
}

stock int PrepareTime(int client, int target, int time, int iCount = 1)
{
	if(target && !IsClientInGame(target))
		return;
	if (!iCount) iCount=1;
	int iDays	= time / 86400	% 365;			// дни
	int iHours	= (time / 3600)	% 24;			// часы
	int iMins	= (time / 60)	% 60;			// минуты
	int iSecs	=	time		% 60;			// секунды
	if (target) {
		char sName[128];
		GetClientName(target, sName, sizeof(sName));
		if(!StrEqual(sName, g_sOldName[target]) && g_sOldName[0][target] == '\0')
			Format(sName, sizeof(sName), "         a.k.a. %s", g_sOldName[target]);
		else Format(sName, sizeof(sName), "");
		PrintToConsole(client, "%2iд %2iч %2iм %2iс - %s %s %s%s %N %s", iDays, iHours, iMins, iSecs, sInGr[target], warden_iswarden(target)?"КМД":(GetClientTeam(target)==3?"КТ ":"   "), (GetUserFlagBits(target) & ADMFLAG_GENERIC)?"A":" ", (GetUserFlagBits(target) & ADMFLAG_CUSTOM1)?"V":" " ,target, sName);
	} else {
		PrintToConsole(client, "%2iд %2iч %2iм %2iс - ИТОГО", iDays, iHours, iMins, iSecs);
		time = RoundToFloor((time/iCount)*1.0);
		iDays	= time / 86400	% 365;			// дни
		iHours	= (time / 3600)	% 24;			// часы
		iMins	= (time / 60)	% 60;			// минуты
		iSecs	=	time		% 60;			// секунды
		PrintToConsole(client, "%2iд %2iч %2iм %2iс - среднее время на человека", iDays, iHours, iMins, iSecs);
	}
}

void GetMainNickName(int iClient, char[] sSteamId)
{		// для получения ника с которым игрок играл дольше всего, если он сменил ник
	if (g_Mysql == INVALID_HANDLE) {	// иногда возникает в момент старта карты
		LogError("g_Mysql == INVALID_HANDLE: for iClient %i, sSteamId = %s",  iClient, sSteamId);
		return;
	}
	ReplaceString(sSteamId, 32, "STEAM_1:", "");
	char Query[512];
	Format(Query, 512, "SELECT name FROM `hlstats_PlayerNames` WHERE (connection_time=( SELECT MAX(connection_time) FROM `hlstats_PlayerNames` WHERE  playerid =( SELECT playerid FROM `hlstats_PlayerUniqueIds` WHERE (game=\"%s\" AND uniqueId =\"%s\") ) ) AND playerid =( SELECT playerid FROM `hlstats_PlayerUniqueIds` WHERE (game=\"%s\" AND uniqueId =\"%s\") ) )", game, sSteamId, game, sSteamId);
	//dbgMsg(Query);
	int iUserId = GetClientUserId(iClient);
	SQL_TQuery(g_Mysql, GetMainNickName_CallBack, Query, iUserId, DBPrio_Normal);
}

int GetMainNickName_CallBack(Handle owner, Handle hndl, const char[] error, any iUserId)
{
	if(hndl == INVALID_HANDLE) {
		LogError("Unable to get player main nickname, reason: %s", error);
		dbgMsg("Unable to get player main nickname, reason: %s", error);
		return;
	}
	int iClient = GetClientOfUserId(iUserId);
	if(iClient){
		if (SQL_FetchRow(hndl)) {
			char sName[64];
			SQL_FetchString(hndl, 0, sName, sizeof(sName));
			//dbgMsg("GetMainNickName_CallBack sName = %s", sName);
			g_sOldName[iClient] = sName;
		}
	}
	return;
}
public APLRes AskPluginLoad2(Handle myself, bool late, char[] error, int err_max)
{
	CreateNative("GetClientPlayedTime",			NativeGetPlayedTime);				//Сколько времени всего наиграл
	RegPluginLibrary("hlstatsX_adv");
	return APLRes_Success;
}

public void OnClientDisconnect(int iClient)
{
	if(iClient && !IsFakeClient(iClient) && !IsClientInGame(iClient))
		dbgMsg("%sза %5.1f сек вышел не подключившись %N", g_iPlayedTime[iClient] == 0? "IsNewbie ":"", GetClientTime(iClient), iClient);
	
	g_iPlayedTime[iClient] = -1;
	g_sOldName[0][iClient] = '\0';
}

public Action Check(Handle timer)
{
	for(int i=1; i<MAXPLAYERS; i++)
		if (g_iPlayedTime[i] == -1 && IsPlayerValid(i)) {
			char steam_id[32];
			GetClientAuthId(i, AuthId_Steam2, steam_id, sizeof(steam_id));
			Call(i, steam_id);
		}
}

public void eV_player_connect(Handle event, const char[] name, bool DB)
{	// первичный запрос
	if(GetEventBool(event, "bot"))
		return;
	int iClient = GetEventInt(event, "index") + 1;
	g_iUId[iClient] = GetEventInt(event, "userid");
	char networkid[32];
	GetEventString(event, "networkid", networkid, sizeof(networkid));
	//dbgMsg("eV_player_connect		Call(%i, %s);  (uid = %i)", iClient, networkid, g_iUId[iClient]);
	// при заходе игрока на сервер Call не будет работать, т.к. игрок будет не в игре, а только будет полкдючаться, однако при смене карты будет работать 
	Call(iClient, networkid);
}

public void OnClientPutInServer(int iClient)
{	// если ответ не пришел
	if (IsFakeClient(iClient))
		return;
	char steam_id[32];
	GetClientAuthId(iClient, AuthId_Steam2, steam_id, sizeof(steam_id));
	if (g_iPlayedTime[iClient] == -1 && IsClientInGame(iClient)) {		// если при смене карты
		// во время смены карты у игрока вызывается OnClientDisconnect поэтому g_iPlayedTime[iClient] становится -1
		// если g_iPlayedTime == -1 и одновременно IsClientInGame значит это переподключение между картами т.к. между картами eV_player_connect не вызывается
		g_iPlayedTime[iClient] = 0;
		//if (!g_bUIDConnd[GetClientUserId(iClient)]) {		// если игрок подключается 1 раз за сессию, а не при смене карты, но такого не может быть
		//	dbgMsg("WARNING!!! PutInServer %L Call after eV_player_connect not worked", iClient);
		//	g_bUIDConnd[GetClientUserId(iClient)] = true;
		//} else 
		//dbgMsg("PutInServer ReCall after map change  iClient %i uid - %i steam_id = %s", iClient, GetClientUserId(iClient), steam_id);
		Call(iClient, steam_id);
	} else {
		if (SteamWorks_HasLicenseForApp(iClient, 730) == k_EUserHasLicenseResultHasLicense) {
			SteamWorks_GetUserGroupStatus(iClient, 103582791431452545);
			if(InGroup(iClient))
				sInGr[iClient] = "[G]";
			 else sInGr[iClient] = "[s]";
		} else sInGr[iClient] = "   ";
		dbgMsg("%s%s %5.1f сек	PlayedTime[%i] = %i		%L", g_iPlayedTime[iClient] == 0? "New ":"    ", sInGr[iClient], GetClientTime(iClient), iClient, g_iPlayedTime[iClient], iClient);
		GetMainNickName(iClient, steam_id);
	}
}

void Call(int iClient, char[] steam_id)
{
	if (g_Mysql == INVALID_HANDLE) {	// иногда возникает в момент старта карты
		LogError("g_Mysql == INVALID_HANDLE: for iClient %i, steam_id = %s",  iClient, steam_id);
		return;
	}
	ReplaceString(steam_id, 32, "STEAM_1:", "");
	char Query[256];
	Format(Query, 512, "SELECT connection_time FROM `hlstats_Players` WHERE playerId IN (SELECT playerId FROM `hlstats_PlayerUniqueIds` WHERE `uniqueId` = '%s' AND `game` = '%s')", steam_id, game);
	SQL_TQuery(g_Mysql, SavePlayedTime, Query, iClient, DBPrio_Normal);
	if (iClient && !IsClientInGame(iClient)) {		// когда игрок уже будет в игре, он уже будет записан в БД и не будет считаться новичком
		Format(Query, 512, "SELECT `uniqueId`, COUNT(*) FROM `hlstats_PlayerUniqueIds` WHERE `uniqueId` = '%s' AND `game` = '%s'", steam_id, game);
		SQL_TQuery(g_Mysql, IsNewbie, Query, iClient, DBPrio_Normal);
	}
	if (IsClientConnected(iClient))
		GetMainNickName(iClient, steam_id);		// вызывается при перезагрузке плагина и смене карты
}

public void SavePlayedTime(Handle owner, Handle hndl, const char[] error, any iClient)
{
	if(hndl == INVALID_HANDLE) {
		dbgMsg("SavePlayedTime(%i)		Unable to load the player, reason: %s", iClient, error);
		LogError("Unable to load the player, reason: %s", error);
		return;
	}
	if(SQL_FetchRow(hndl))
		g_iPlayedTime[iClient] = SQL_FetchInt(hndl, 0);
	//dbgMsg("SavePlayedTime(%i)		g_iPlayedTime[%i] = %i", iClient, iClient, g_iPlayedTime[iClient]);
	return;
}

public void IsNewbie(Handle owner, Handle hndl, const char[] error, any iClient)
{
	if(hndl == INVALID_HANDLE) {
		LogError("Unable to load the player, reason: %s", error);
		return;
	}
	if(SQL_FetchRow(hndl) && !SQL_FetchInt(hndl, 1)) {		// значит игрок новичок
		//dbgMsg("true IsNewbie %L g_iPlayedTime[iClient] = %i", iClient, g_iPlayedTime[iClient]);
		if (g_iPlayedTime[iClient] == -1) {		// IsPlayerValid(iClient) не будет работать, т.к. вызов идет после eV_player_connect когда игрок ещё не в игре, а после OnClientPutInServer он уже прописывается в БД и считается не как новичок
			g_iPlayedTime[iClient] = 0;
			g_bUIDConnd[g_iUId[iClient]] = true;
		} else dbgMsg("WARNING!!!! true IsNewbie %L g_iPlayedTime[iClient] = %i", iClient, g_iPlayedTime[iClient]);
	}// else dbgMsg("false IsNewbie %L", iClient);
	return;
}

public int NativeGetPlayedTime(Handle plugin, int numParams)
{
	int iClient = GetNativeCell(1);
	if(!IsPlayerValid(iClient))
		ThrowNativeError(SP_ERROR_INDEX, "Client index %i is invalid", iClient);
	if (g_iPlayedTime[iClient] == -1)
		return -1;
	int played_time = RoundToCeil(GetClientTime(iClient)) + g_iPlayedTime[iClient];
	return played_time;
}

/*Connecting...*/
void ConnectToMySql()
{
	if (g_Mysql != INVALID_HANDLE) {
		CloseHandle(g_Mysql);
		g_Mysql = INVALID_HANDLE;
	}
	
	if (SQL_CheckConfig("hlstatsX"))
		SQL_TConnect(CallBackConnectToDB, "hlstatsX");
	else LogError("[ConnectToMySql] Can't find 'hlstatsX' in databases.cfg");
}


public void CallBackConnectToDB(Handle owner, Handle hndl, const char[] error, any data)
{
	if (hndl == INVALID_HANDLE)
		LogError("[CallBackConnectToDB] Can't find 'hlstatsX' in databases.cfg");

	g_Mysql = CloneHandle(hndl);
}

bool IsPlayerValid(int iClient)
{
	return (!iClient || !IsClientInGame(iClient) || IsFakeClient(iClient)) ? false:true;
}